#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Data Visualization

A collection of modules used in data visualization scripts

Installation

To install the latest version of these python libraries:

    navigate to this folder
    pip install .

"""

# import modules
from setuptools import setup, find_packages

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name="datavisualization",
    version="0.0.1",
    description="Python tools for data visualization",
    long_description=long_description,
    long_description_content_type="text/markdown",

    url="https://git.app.uib.no/Guttorm.Alendal/dcod-at-runde/blob/master/lecturers/morten",
    author="Morten Brun",
    author_email="morten.brun@uib.no",
    license="GPL-3",
    packages=find_packages(),
    classifiers=[
        # How mature is this project? Common values are
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        'Development Status :: 3 - Alpha',

        # Indicate who your project is intended for
        'Intended Audience :: Data scientists',
        'Topic :: Data visualization :: Data analysis',

        # Pick your license as you wish (should match "license" above)
        'License :: GPL-3',

        # Specify the Python versions you support here. In particular, ensure
        # that you indicate whether you support Python 2, Python 3 or both.
        # 'Programming Language :: Python :: 2',
        # 'Programming Language :: Python :: 2.6',
        # 'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        # 'Programming Language :: Python :: 3.2',
        # 'Programming Language :: Python :: 3.3',
        # 'Programming Language :: Python :: 3.4'
        "Operating System :: OS Independent",
    ],
    keywords="visualization",
    install_requires=["numpy", "scipy", "pandas",
                      "umap-learn", "xlrd", "hdbscan",
                      "matplotlib", "scikit-learn"]
)
